* improve unit test coverage
* improve README.rst
* improve docstrings
* improve argument parsing in command\_line.py
 
