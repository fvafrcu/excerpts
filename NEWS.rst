2.1.0 (2019-03-31)
==================

- Adjusted order of imports.
- Moved from github to gitlab.

2.0.0 (2017-06-16)
==================

- Allow formats for pandoc to be a string, tuple or vector. Formerly only a
  (comma separated) string was implemented.

1.2.0 (2017-05-21)
==================

- Enhance documentation.

1.1.0 (2017-05-21)
==================

- Switch to subprocess.check_call().

1.0.0 (2017-04-07)
==================

- Initial Release.


